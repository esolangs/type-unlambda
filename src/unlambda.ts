/**
 * unlambda.ts - Unlambda interpreter entry.
 * 
 * @author CismonX <admin@cismon.net>
 * @license MIT
 */

import { Parse, ParseResult } from './parser';
import { Eval } from './runtime';

/**
 * Get the parse result expression, or `never` if parse failed.
 */
type ParseResultValue<T extends ParseResult> =
    Parse<T[1]> extends [never, ''] ? T[0] : never;

/**
 * Get the output string of an evalalutation result, or `never`
 * if evaluation failed.
 */
type EvalResultOutput<T> = T extends [any, [any, infer O, any]] ? O : never;

/**
 * Given the source code of an Unlambda program, and input string.
 * 
 * Returns the output of program execution, or `never` if something went wrong.
 */
type Unlambda<Code extends string, Input extends string = ''> = 
    EvalResultOutput<Eval<ParseResultValue<Parse<Code>>, [], [Input, '', '']>>;

export default Unlambda;
